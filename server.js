const express = require('express');
const app = express();
const server = require('http').Server(app);
const cors = require('cors');
const bodyParser = require('body-parser');
const MusicLibrary = require('./music_library');

// ================= CONFIG ===================
const MUSIC_FOLDER_PATH = 'E:/Users/TooL/Music';
const PORT = 7000;
// ============================================

const musicLibrary = new MusicLibrary({ root: MUSIC_FOLDER_PATH });

app.use(cors());
app.use(bodyParser.json());
app.use('/', express.static(__dirname));

require('./api')(app, musicLibrary);

const onMusicLibraryReady = () => {
  server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}...`);
    musicLibrary.removeListener('ready', onMusicLibraryReady);
  });
};

musicLibrary.on('ready', onMusicLibraryReady);
