# HomeMusic.NodeApi
***

### This is web server for streaming music tracks over network, written by me in Node.js.

## Usage:
***
#### In order to use the server, after *HomeMusic.NodeApi* is up and running, setup and run [*HomeMusic*](https://bitbucket.org/mroje/homemusic/src/master/) app.

### Server setup:
1. In **server.js**, change *MUSIC_FOLDER_PATH* constant to your local music folder path which will be used as the root folder.
2. Root folder needs to contain folders named after bands (those will be listed under "Bands" in [*HomeMusic*](https://bitbucket.org/mroje/homemusic/src/master/) app). Band folders can contain both folders and song files, structure doesn't matter.
3. Using terminal navigate to project directory and run **npm start** command to run the server.