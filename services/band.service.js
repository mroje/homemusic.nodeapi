module.exports = musicLibrary => {
  
  getAll = () => {
    const bands = musicLibrary.getAllBands();
    return bands;
  };

  getById = id => {
    const band = musicLibrary.getBandById(id);
    if (!band) { return; }

    return band;
  };

  return {
    getAll,
    getById
  };
};
