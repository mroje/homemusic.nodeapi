const fuzzy = require('fuzzy');

module.exports = musicLibrary => {

  getTrackPath = id => {
    let track = musicLibrary.getTrackById(id);
    if (!track) { return; }

    return track.filePath;
  };

  getFavorites = () => {
    const allTracks = musicLibrary.getAllTracks();
    return allTracks.filter(track => track.isFavorite);
  };

  toggleFavorite = id => {
    musicLibrary.toggleFavoriteTrack(id);
  };

  getTracksBySearchTerm = searchTerm => {
    let allTracks = musicLibrary.getAllTracks();

    let filteredTracks = fuzzy
      .filter(searchTerm, allTracks, { extract: el => { return el.name; } })
      .map(obj => obj.original);

    return filteredTracks;
  };

  return {
    getTrackPath,
    getFavorites,
    toggleFavorite,
    getTracksBySearchTerm
  };
};
