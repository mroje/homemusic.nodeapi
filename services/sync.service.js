module.exports = musicLibrary => {

  sync = async () => {
    return new Promise((resolve, reject) => {

      const onMusicLibraryPopulate = () => {
        musicLibrary.removeListener('ready', onMusicLibraryPopulate);
        resolve();
      };
      
      musicLibrary.on('ready', onMusicLibraryPopulate);
  
      musicLibrary.clear();
      musicLibrary.populate();
    });
  };

  return {
    sync
  }
};
