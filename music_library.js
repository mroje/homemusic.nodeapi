const redis = require('redis');
const jsonify = require('redis-jsonify');
const client = jsonify(redis.createClient());

const EventEmitter = require('events').EventEmitter;
const fs = require('fs');
const path = require('path');
const util = require('util');
const find = require('findit');
const _ = require('lodash');

const audioFormats = ['.mp3'];

function* incrementer (i) {
  while (true) { 
    i++;
	  yield i;
	}
}

let bandIdGenerator = incrementer(0);
let trackIdGenerator = incrementer(0);

class Track {
  constructor(name, filePath, bandId) {
    this.id = trackIdGenerator.next().value;

    this.name = name;
    this.filePath = filePath;
    this.bandId = bandId;

    this.isFavorite = false;
  }
}

class Band {
  constructor(name) {
    this.id = bandIdGenerator.next().value;

    this.name = name;
  }
}

function MusicLibrary(opts) {
  EventEmitter.call(this);

  if (!opts || typeof opts !== 'object') {
    throw new Error('MusicLibrary - Root path not specified (must be of type object)');
  }

  this.opts = opts;
  this.bands = [];
  this.tracks = [];

  this.populate();
}

util.inherits(MusicLibrary, EventEmitter);

MusicLibrary.prototype.populate = function () {

  this.getAudioFilePaths = rootPath => {
    return new Promise((resolve, reject) => {
      let filePaths = [];
      const finder = find(rootPath);

      finder.on('file', (fpath, stat) => {
        const extension = path.extname(fpath);
        if (_.includes(audioFormats, extension) && stat.size) {
          filePaths.push(fpath);
        }
      });

      finder.on('end', () => {
        resolve(filePaths);
      });
    });
  };

  let promises = [];

  const getDirectories = srcPath => fs
    .readdirSync(srcPath)
    .filter(file => fs.statSync(path.join(srcPath, file))
    .isDirectory());

  const bandDirectoryPaths = getDirectories(this.opts.root)
    .map(directoryName => `${this.opts.root}/${directoryName}`);

  bandDirectoryPaths.forEach(bandPath => {
    let promise = new Promise((resolve, reject) => {
      this.getAudioFilePaths(bandPath)
        .then(trackPaths => {
          const bandName = bandPath.substring(bandPath.lastIndexOf('/') + 1);

          let band = new Band(bandName);

          let bandTracks = trackPaths.map(trackPath => {
            const trackName = trackPath.substring(trackPath.lastIndexOf('\\') + 1);

            let track = new Track(trackName, trackPath, band.id);
            this.tracks.push(track);

            return track;
          });

          band.tracks = bandTracks;
          this.bands.push(band);

          resolve();
        });
    });

    promises.push(promise);
  });

  Promise.all(promises)
    .then(() => {
      this.initFavoriteTracks();
      this.emit('ready');
    });

  this.getAllTracks = () => {
    return this.tracks;
  };

  this.getAllBands = () => {
    return this.bands;
  };

  this.getBandById = id => {
    const band = this.bands.find(b => b.id === id);
    return band;
  };

  this.getTrackById = id => {
    const track = this.tracks.find(t => t.id === id);
    return track;
  };

  this.toggleFavoriteTrack = id => {
    const track = this.tracks.find(t => t.id === id);
    track.isFavorite = !track.isFavorite;

    this.updateDatabaseFavoriteTracks(track);
  };

  this.initFavoriteTracks = () => {
    client.get('favorite_tracks', (err, result) => {
      this.favoriteTrackPaths = result || {};

      this.tracks.forEach(track => {
        if (this.favoriteTrackPaths[track.filePath]) {
          track.isFavorite = true;
        }
      });
    });
  };

  this.updateDatabaseFavoriteTracks = toggledTrack => {
    const existingFavoriteTrackPath = this.favoriteTrackPaths[toggledTrack.filePath];

    if (toggledTrack.isFavorite && !existingFavoriteTrackPath) {
      this.favoriteTrackPaths[toggledTrack.filePath] = true;
    }
    else if (!toggledTrack.isFavorite && existingFavoriteTrackPath) {
      delete this.favoriteTrackPaths[toggledTrack.filePath];
    }

    client.set('favorite_tracks', this.favoriteTrackPaths, (err, result) => {
      if (err) { console.error(err); }
    });
  };
};

MusicLibrary.prototype.clear = function () {
  this.bands = [];
  this.tracks = [];
}

module.exports = MusicLibrary;
