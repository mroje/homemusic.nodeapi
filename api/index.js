const bandsApi = require('./bands.api');
const tracksApi = require('./tracks.api');
const syncApi = require('./sync.api');

const BandService = require('./../services/band.service');
const TrackService = require('./../services/track.service');
const SyncService = require('./../services/sync.service');

module.exports = (app, musicLibrary) => {

  const bandService = BandService(musicLibrary);
  const trackService = TrackService(musicLibrary);
  const syncService = SyncService(musicLibrary);

  bandsApi(app, bandService);
  tracksApi(app, trackService);
  syncApi(app, syncService);
};
