module.exports = (app, bandService) => {

  app.get('/api/bands', (req, res) => {
    let bands = bandService.getAll();
    res.status(200).send(bands);
  });

  app.get('/api/bands/:id', (req, res) => {
    const bandId = parseInt(req.params.id);
    const band = bandService.getById(bandId);

    if (!band) {
      return res.status(404).send();
    }
    
    res.status(200).send(band);
  });
};
