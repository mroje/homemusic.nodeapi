const fs = require('fs');
const pathExists = require('path-exists');

module.exports = (app, trackService) => {

  app.get('/api/tracks/:id/stream', (req, res) => {
    const trackId = parseInt(req.params.id);
    const trackPath = trackService.getTrackPath(trackId);

    pathExists(trackPath).then(exists => {
      if (!exists) {
        console.error('File `' + trackPath + '` not found');
        res.writeHead(404);
        res.end(msg);
        return;
      }
      fs.stat(trackPath, (err, stats) => {
        if (err) {
          console.error(err);
          res.writeHead(500);
          return;
        }
        res.writeHead(200, {
          'Content-Type': 'audio/mpeg',
          'Content-Length': stats.size,
          'Accept-Ranges': 'bytes'
        });

        let readStream = fs.createReadStream(trackPath);
        readStream.pipe(res);
      });
    });
  });

  app.get('/api/tracks/search', (req, res) => {
    const searchTerm = req.query.term;
    const tracks = trackService.getTracksBySearchTerm(searchTerm);

    res.status(200).send(tracks);
  });

  app.get('/api/tracks/favorites', (req, res) => {
    const favoriteTracks = trackService.getFavorites();
    res.status(200).send(favoriteTracks);
  });

  app.post('/api/tracks/:id/toggle-favorite', (req, res) => {
    const trackId = parseInt(req.params.id);
    trackService.toggleFavorite(trackId);

    res.status(200).send();
  });
};
