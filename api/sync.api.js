module.exports = (app, syncService) => {

  app.get('/api/sync', async (req, res) => {
    await syncService.sync();
    
    res.status(200).send();
  });
};
